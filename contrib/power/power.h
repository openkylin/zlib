/* Copyright (C) 2019 Matheus Castanho <msc@linux.ibm.com>, IBM
 *               2019 Rogerio Alves    <rogerio.alves@ibm.com>, IBM
 * For conditions of distribution and use, see copyright notice in zlib.h
 */

#include "../../zconf.h"

unsigned long _crc32_z_power8(unsigned long, const Bytef *, z_size_t);
